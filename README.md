## <font size="7">**Kanvas**</font>

### Features

- [x] Cadastro de usuário
- [x] Cadastro de Instrutor
- [x] Faz a autenticação do usuário
- [x] Cria um curso
- [x] Lista os cursos e os alunos matriculados
- [x] Retorna o curso com o id informado
- [x] Vincula os alunos ao curso
- [x] Vincula os instrutores ao curso
- [x] Cria um novo endereço para o usuario, caso nao exista.
- [x] Lista os endereços

## <font size="6">Routes</font>

### <font color="red"> POST </font> Create user

```json
/api/accounts/
```

```json
{
  "username": "Instrutor Carlos",
  "password": "1234",
  "is_admin": true
}
```

​
<font color="white"> _Response_ </font>
​

```json
{
  "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
  "username": "Instrutor Carlos",
  "is_admin": true
}
```

### <font color="red"> POST </font> sign in

```json
/api/login/
```

```json
{
  "username": "Facilitador Pedro",
  "password": "1234"
}
```

<font color="white"> _Response_ </font>
​

```json
{
  "token": "4a18f48640e97cadf32137e480be3ec8d0b230b8"
}
```

### <font color="red"> POST </font> Create a course

```json
/api/courses/
```

```json
{
  "name": "Django Rest Framework"
}
```

<font color="white"> _Response_ </font>
​

```json
{
  "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb": 3,
  "name": "Django Rest Framework",
  "students": [],
  "instructor": null

}

```

### <font color="red"> GET </font> List all courses, instructor and students

```json
/api/courses/
```

```json
[
  {
    "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
    "name": "Django 3.22",
    "students": [
        {
      "uuid": "01a81bc7-7a59-442d-87a6-020884cad916",
      "first_name": "John",
      "email": "jon@mail.com",
      "is_admin": false
    },
    {
      "uuid": "08ff12ac-a4ac-43b1-be2a-a0b9b8cb9c13",
      "first_name": "Mike",
      "email": "mike@mail.com",
      "is_admin": false
    },
    {
      "uuid": "976d0604-30b3-48bb-8bd9-232644aadda5",
      "first_name": "Billy",
      "email": "billy@mail.com",
      "is_admin": false
    }
    ],
     "instructor": {
      "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
      "first_name": "Mary",
      "email": "mary@mail.com",
      "is_admin": true
  },
  {
    "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
    "name": "Node.JS 3.2.8",
    "users": []
  }
```

### <font color="red"> GET </font> get course by id

```json
/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/
```

```json
{
  "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
  "name": "Node.JS 3.2.8",
  "users": []
}
```

### <font color="red"> PUT </font> Registrate Student in course

```json
/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/registrations/students
```

```json
{
{
  "students_ids":  ["0d54b502-17c8-4a20-b90f-d5ecc125e3bb", "d8be7375-bd6c-4de8-b2a6-a439d9e2cb7f"
  ]
}
}
```

<font color="white"> _Response_ </font>
​

```json
/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/registrations/students
```

```json
{
{
    "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
    "name": "Django 3.22",
    "demo_time": "11:40",
    "students" : [
        {
      "uuid": "01a81bc7-7a59-442d-87a6-020884cad916",
      "first_name": "John",
      "email": "jon@mail.com",
      "is_admin": false
    },
    {
      "uuid": "08ff12ac-a4ac-43b1-be2a-a0b9b8cb9c13",
      "first_name": "Mike",
      "email": "mike@mail.com",
      "is_admin": false
    },
    {
      "uuid": "976d0604-30b3-48bb-8bd9-232644aadda5",
      "first_name": "Billy",
      "email": "billy@mail.com",
      "is_admin": false
    }
    ],
     "instructor": {
      "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
      "first_name": "Mary",
      "email": "mary@mail.com",
      "is_admin": true
     }
}
}
}
```

### <font color="red"> PUT </font> Registrate Instructor in course

```json
/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/registrations/instructor
```

```json
{
{
  "instructor_id":  "0d54b502-17c8-4a20-b90f-d5ecc125e3bb"
}
}
```

<font color="white"> _Response_ </font>
​

```json
/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/registrations/instructor
```

```json
{
  "uuid": "d8be7375-bd6c-4de8-b2a6-a439d9e2cb7f",
  "name": "NodeJs",
  "demo_time": "11:45:00",
  "link_repo": "https://gitlab.com/js-t2/",
  "students": [],
  "instructor": {
    "uuid": "0d54b502-17c8-4a20-b90f-d5ecc125e3bb",
    "first_name": "Mary",
    "email": "mary@mail.com",
    "is_admin": true
  }
}
```

### <font color="red"> PATCH </font> Patch course

```json
	/api/courses/<str:course_id>/
```

```json
{
  "demo_time": "11:30"
}
```

<font color="white"> _Response_ </font>

````json
	/api/courses/<str:course_id>/
```​
```json
{
    "uuid": "d8be7375-bd6c-4de8-b2a6-a439d9e2cb7f",
  "name": "NodeJs",
  "demo_time": "11:30:00",
  "link_repo": "https://gitlab.com/js-t2/",
  "students": [],
  "instructor": null
}
````

### <font color="red"> DELETE </font> Delete course **\***

```json
	/api/courses/0d54b502-17c8-4a20-b90f-d5ecc125e3bb/
```

### <font color="red"> POST </font> Create an address

```json
/api/address/
```

```json
{
  "street": "Rua A",
  "house_number": 121,
  "city": "Cidade D",
  "state": "Estado C",
  "zip_code": "94444-444",
  "country": "País F"
}
```

<font color="white"> _Response_ </font>
​

```json
{
    "uuid": "af1b0cd0-72cd-4f6f-b090-549eb8564605",
    "street": "Rua A",
    "house_number": 121,
    "city": "Cidade D",
    "state": "Estado C",
    "zip_code": "94444-444",
    "country": "País F",
    "users": []
  },
  {
    "uuid": "ca455151-414d-47e0-a729-c156e6e62ca6",
    "street": "Rua D",
    "house_number": 121,
    "city": "Cidade D",
    "state": "Estado C",
    "zip_code": "94474-444",
    "country": "País F",
    "users": [
      {
        "uuid": "08ff12ac-a4ac-43b1-be2a-a0b9b8cb9c13",
        "first_name": "Mike",
        "email": "mike@mail.com",
        "is_admin": false
      }
    ]
  }
```
