from django.urls import path
from .views import CourseView, SpecificCourseView, RegisterInstructorView, RegisterStudentView

urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/<str:course_id>/', SpecificCourseView.as_view()),
    path('courses/<str:course_id>/registrations/students/', RegisterStudentView.as_view()),
    path('courses/<str:course_id>/registrations/instructor/', RegisterInstructorView.as_view()),
]