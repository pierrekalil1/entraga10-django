from django.db import models
import uuid


class Course(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, unique=True)
    demo_time = models.TimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    link_repo = models.CharField(max_length=255)
    instructor = models.OneToOneField('User.User', related_name='course', on_delete=models.CASCADE, null=True)
    students = models.ManyToManyField('User.User', related_name='courses')