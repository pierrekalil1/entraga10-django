from rest_framework.permissions import BasePermission

class IsAdminCreation(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        return request.user.is_authenticated and request.user.is_admin == True
    
class UserPermission(BasePermission):
    def has_permission(self, request, view):

        return request.user.is_admin == False