from django.db import IntegrityError
from django.forms import ValidationError
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from django.core.exceptions import ObjectDoesNotExist
from .models import Course
from User.models import User
from .serializers import CourseSerializer, StudentSerializer
from .permissions import CoursePermission, SpecifCoursePermisson
from rest_framework.decorators import authentication_classes, permission_classes


class CourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [CoursePermission]

    def get(self, request):
        courses = Course.objects.all()
        courses = CourseSerializer(courses, many=True)

        return Response(courses.data, status=status.HTTP_200_OK)

    def post(self, request):

        serializer = CourseSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        course_already_exists = Course.objects.filter(
            name=request.data["name"]
        ).exists()

        if course_already_exists:
            return Response(
                {"message": "Course already exists"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        course = Course.objects.create(**serializer.validated_data)
        serializer = CourseSerializer(course)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class SpecificCourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [SpecifCoursePermisson]

    def get(self, request, course_id):
        try:
            course = Course.objects.get(uuid=course_id)

            serializer = CourseSerializer(course)

            return Response(serializer.data, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response(
                {"message": "Course does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

    def patch(self, request, course_id=""):
        try:
            course = Course.objects.get(uuid=course_id)

            serializer = CourseSerializer(data=request.data, partial=True)
            serializer.is_valid()

            data = {**serializer.validated_data}

            for item in data.keys():
                course.__dict__[item] = data[item]

            course.save()

            course = Course.objects.get(uuid=course_id)
            serializer = CourseSerializer(course)

            return Response(serializer.data)

        except Course.DoesNotExist:
            return Response(
                {"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND
            )
        except IntegrityError:
            return Response(
                {"message": "This course name already exists"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

    def delete(self, request, course_id=""):

        try:
            course = Course.objects.get(uuid=course_id)
            course.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)
        except Course.DoesNotExist:
            return Response(
                {"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND
            )


class RegisterStudentView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [SpecifCoursePermisson]

    def put(self, request, course_id):
        try:

            validate = StudentSerializer(data=request.data)
            if not validate.is_valid():
                return Response(validate.errors, status=status.HTTP_400_BAD_REQUEST)

            course = Course.objects.get(uuid=course_id)
            course.students.clear()

            for id in request.data["students_id"]:
                student = User.objects.get(uuid=id)
                if student.is_admin:
                    return Response(
                        {"message": "Some student id belongs to an Instructor"},
                        status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    )

                course.students.add(student)
                course.save()

            serializer = CourseSerializer(course)

            return Response(serializer.data, status=status.HTTP_200_OK)

        except Course.DoesNotExist:
            return Response(
                {"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND
            )

        except User.DoesNotExist:
            return Response(
                {"message": "Invalid students_id list"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except ValidationError:
            return Response(
                {"message": "Invalid students_id list"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class RegisterInstructorView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [SpecifCoursePermisson]

    def put(self, request, course_id):
        try:
            course = Course.objects.get(uuid=course_id)

            instructor = User.objects.get(uuid=request.data["instructor_id"])

            instructor_registered = Course.objects.all()

            if instructor_registered:
                for key in instructor_registered:
                    if key.instructor == instructor:
                        key.instructor = None
                        key.save()

            course.instructor = instructor

            if instructor.is_admin != True:
                return Response(
                    {"message": "Instructor id does not belong to an admin"},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY,
                )

            course.instructor = instructor
            course.save()

            serializer = CourseSerializer(course)

            return Response(serializer.data, status=status.HTTP_200_OK)

        except Course.DoesNotExist:
            return Response(
                {"message": "Course does not exist"}, status=status.HTTP_404_NOT_FOUND
            )
        except User.DoesNotExist:
            return Response(
                {"message": "Invalid instructor_id"}, status=status.HTTP_404_NOT_FOUND
            )
        except KeyError:
            return Response(
                "instructor_id is a required field.", status=status.HTTP_400_BAD_REQUEST
            )

        if instructor.is_admin == False:
            return Response(
                {"message": "Instructor id does not belong to an admin"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        course.save()

        serializer = CourseSerializer(course)

        return Response(serializer.data)
