from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

# from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import User, Address
from .serializers import UserSerializer, LoginSerializer, AddressSerializer
from rest_framework.authentication import authenticate, TokenAuthentication
from rest_framework.authtoken.models import Token
from .permissions import IsAdminCreation
from rest_framework.decorators import authentication_classes, permission_classes


class UserView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminCreation]

    def post(self, request):

        serializer = UserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user_already_registered = User.objects.filter(
            email=request.data["email"]
        ).exists()

        if user_already_registered:
            return Response(
                {"message": "User already exists"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        user = User.objects.create_user(**serializer.validated_data)

        serializer = UserSerializer(user)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request):

        users = User.objects.all()

        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LoginView(APIView):
    def post(self, request):
        try:
            serializer = LoginSerializer(data=request.data)

            serializer.is_valid()

            email = request.data["email"]
            password = request.data["password"]

        except KeyError:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=email, password=password)

        if user:
            token = Token.objects.get_or_create(user=user)[0]

            return Response({"token": token.key})

        return Response(
            {"message": "Invalid Credentials"}, status=status.HTTP_401_UNAUTHORIZED
        )


class AddressView(APIView):

    authentication_classes = [TokenAuthentication]

    def put(self, request):

        user = User.objects.get(uuid=request.user.uuid)

        serializer = AddressSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            address = Address.objects.get(zip_code=request.data["zip_code"])

        except Address.DoesNotExist:
            address, created = Address.objects.update_or_create(
                zip_code=request.data.get("zip_code"),
                street=request.data.get("street"),
                house_number=request.data.get("house_number"),
                city=request.data.get("city"),
                state=request.data.get("state"),
                country=request.data.get("country"),
            )

        user = request.user

        user.address = address
        user.save()

        serializer = AddressSerializer(address)
        return Response(serializer.data)

    def get(self, request):

        address = Address.objects.all()

        serializer = AddressSerializer(address, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
