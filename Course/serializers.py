from rest_framework import serializers
from User.serializers import UserSerializer


class CourseSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    demo_time = serializers.TimeField()
    created_at = serializers.DateTimeField(read_only=True)
    link_repo = serializers.CharField()
    instructor = UserSerializer(read_only=True)
    students = UserSerializer(many=True, read_only=True)


class StudentSerializer(serializers.Serializer):
    students_id = serializers.ListField()
