from django.urls import path
from .views import UserView, LoginView, AddressView

urlpatterns = [
    path('accounts/', UserView.as_view()),
    path('login/', LoginView.as_view()),
    path('address/', AddressView.as_view())
]
